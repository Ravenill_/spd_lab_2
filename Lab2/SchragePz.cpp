#include "SchragePz.h"
#include <queue>
#include "LambdaComparators.h"

int SchragePz::sort(std::vector<Task> _taskVector)
{
	int t = 0;
	int cMax = 0;
	int dummyArg = 99999999;
	Task l;

	std::priority_queue<Task, std::vector<Task>, decltype(decQ)> G(decQ);
	std::priority_queue<Task, std::vector<Task>, decltype(incR)> N(incR);

	for (auto& elem : _taskVector)
		N.push(elem);

	while (!G.empty() || !N.empty())
	{
		while (!N.empty() && N.top().r <= t)
		{
			G.push(N.top());
			
			if (N.top().q > l.q)
			{
				l.p = t - N.top().r;
				t = N.top().r;

				if (l.p > 0)
				{
					G.push(l);
				}
			}

			N.pop();
		}

		if (G.empty())
		{
			t = N.top().r;
		}
		else
		{
			l = G.top();
			t += G.top().p;
			cMax = (cMax > (t + G.top().q)) ? cMax : (t + G.top().q);
			G.pop();
		}
	}

	return cMax;
}
