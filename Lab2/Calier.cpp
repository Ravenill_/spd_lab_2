#include "Calier.h"
#include "SchragePz.h"
#include "Schrage.h"
#include "CmaxFinder.h"
#include <iostream>

int Calier::sort(std::vector<Task>& _taskVector)
{
	std::vector<Task> helper(_taskVector);
	Calier::sort(_taskVector, helper, 999999);
	return CmaxFinder::findCmax(_taskVector, _taskVector.size());
}

void Calier::sort(std::vector<Task>& _taskVector, std::vector<Task>& helper, int _UB)
{
    const int U = Schrage::sort(helper);
	int UB = _UB;
	int LB = 0;

	if (U < UB)
	{
		UB = U;
		_taskVector = helper;
	}

	const int b = findB(U, helper);
	const int a = findA(U, b, helper);
	const int c = findC(a, b, helper);

    if (c == -1)
    {
        return;
    }
		
	int temp_r = helper[c + 1].r;
	int temp_p = 0;
	int temp_q = helper[c + 1].q;

	for (int i = c + 1; i <= b; i++)
	{
		temp_p = temp_p + helper[i].p;

		if (helper[i].r < temp_r)
			temp_r = helper[i].r;

		if (helper[i].q > temp_q)
			temp_q = helper[i].q;
	}

	int r_container = helper[c].r;
	helper[c].r = (r_container > (temp_r + temp_p)) ? r_container : (temp_r + temp_p);

	LB = SchragePz::sort(helper);

	if ((LB < UB) && (helper[c].r != r_container || UB > U))
	{
        //std::cerr << "In, Glebokosc: " << ++licznik << "\n";
        sort(_taskVector, helper, UB);
        //std::cerr << "Out, Glebokosc: " << licznik-- << "\n";
	}
	helper[c].r = r_container;

	int q_container = helper[c].q;
	helper[c].q = (q_container > (temp_q + temp_p)) ? q_container : (temp_q + temp_p);

	LB = SchragePz::sort(helper);

	if ((LB < UB) && (helper[c].r != q_container || UB > U))
	{
        //std::cerr << "In, Glebokosc: " << ++licznik << "\n";
        sort(_taskVector, helper, UB);
        //std::cerr << "Out, Glebokosc: " << licznik-- << "\n";
	}
	helper[c].q = q_container;

    return;
}

int Calier::findA(const int cMaxSchrage, const int B, const std::vector<Task>& _vector)
{
	int temp_value = 0;
	int a = -1;

	for (int i = 0; i <= B; i++)
	{
		temp_value = _vector[i].r;
		for (int j = i; j <= B; j++)
		{
			temp_value = temp_value + _vector[j].p;
		}
		temp_value = temp_value + _vector[B].q;

		if (cMaxSchrage == temp_value)
		{
			a = i;
			return a;
		}
	}
	return -1;
}

int Calier::findB(const int cMaxSchrage, const std::vector<Task>& _vector)
{
	int C = _vector[0].r;
	int b = -1;
	for (unsigned int i = 0; i < _vector.size(); i++)
	{
		C = C + _vector[i].p;

		if (cMaxSchrage == (C + _vector[i].q))
		{
			b = i;
			return b;
		}

		if (C < _vector[i + 1].r)
			C = _vector[i + 1].r;
	}
	return -1;
}

int Calier::findC(const int A, const int B, const std::vector<Task>& _vector)
{
	int c = -1;
	int temp_value = _vector[A].q;
	int border = _vector[B].q;

	for (int i = A; i <= B; i++)
	{
		int temp_q = _vector[i].q;
		if ((temp_q > temp_value) && (temp_q < border))
		{
			temp_value = temp_q;
			c = i;
		}
	}
	return c;
}
