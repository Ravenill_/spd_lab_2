#pragma once
#include <vector>
#include "Task.h"

static int licznik = 0;

class Calier
{
private:
	static void sort(std::vector<Task>& _taskVector, std::vector<Task>& helper, int UB);
	static int findA(const int cMaxSchrage, const int B, const std::vector<Task>& _vector);
	static int findB(const int cMaxSchrage, const std::vector<Task>& vector);
	static int findC(const int A, const int B, const std::vector<Task>& _vector);

public:
	static int sort(std::vector<Task>& _taskVector);
};