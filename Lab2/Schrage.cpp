#include "Schrage.h"
#include <queue>
#include "LambdaComparators.h"

int Schrage::sort(std::vector<Task>& _taskVector)
{
	int t = 0;
	int cMax = 0;

	std::priority_queue<Task, std::vector<Task>, decltype(decQ)> G(decQ);
	std::priority_queue<Task, std::vector<Task>, decltype(incR)> N(incR);

	for (auto& elem : _taskVector)
		N.push(elem);

	_taskVector.clear();

	while(!G.empty() || !N.empty())
	{
		while (!N.empty() && N.top().r <= t)
		{
			G.push(N.top());
			N.pop();
		}

		if (G.empty())
		{
			t = N.top().r;
		}
		else
		{
			_taskVector.push_back(G.top());
			t += G.top().p;
			cMax = (cMax > (t + G.top().q)) ? cMax : (t + G.top().q);
			G.pop();
		}
	}

	return cMax;
}
