#include "SortR.h"
#include <algorithm>

void SortR::sort(std::vector<Task>& obj)
{
	std::sort(obj.begin(), obj.end(), [](const Task& a, const Task& b)->bool { return a.r < b.r; } );
}
