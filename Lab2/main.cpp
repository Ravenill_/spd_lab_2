#include "Reader.h"
#include "CmaxFinder.h"
#include <cstdlib>
#include "SortR.h"
#include "Schrage.h"
#include "SchragePz.h"
#include "Calier.h"

void printCmax()
{
	std::cout << "\n";
	std::cout << "Value of Cmax: ";
}

int main()
{
	std::vector<Task> taskVect; //wektor, kt�ry przechowuje taski

	Reader reader; //obiekt czytaj�cy z pliku. W konstruktorze mo�esz poda� jego nazw�.
	reader.readFromFileTo(taskVect); //fatkycznie wczytanie z pliku do wektora

	std::vector<Task> taskVect2(taskVect);
	/*
	std::cout << "Loaded data:\n";
	for (auto& obj : taskVect) //zwyk�e wy�wietlenie
		std::cout << obj;
	*/
	std::cout << "\n\n________\nSchragePz  cMax: " << SchragePz::sort(taskVect) << "\n________\n\n";

	std::cout << "\n\n________\nSchrage  cMax: " << Schrage::sort(taskVect) << "\n________\n\n";

	std::cout << "\n\n________\nCalier  cMax: " << Calier::sort(taskVect2) << "\n________\n\n";
	/*
	std::cout << "Sorted data:\n";
	for (auto& obj : taskVect) //zwyk�e wy�wietlenie
		std::cout << obj;
	*/
	system("PAUSE");
}
