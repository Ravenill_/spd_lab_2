#pragma once
#include <iostream>

class Task
{
public:
	int r;
	int p;
	int q;

	Task();
	Task(int _r, int _p, int _q) : r(_r), p(_p), q(_q) {}

	friend std::istream& operator >> (std::istream& inputStream, Task& obj);
	friend std::ostream& operator << (std::ostream& outputStream, Task& obj);
	Task& operator =(const Task& arg);
};