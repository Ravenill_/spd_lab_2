#pragma once
#include "Task.h"

const auto incR = [](const Task& a, const Task& b)->bool { return a.r > b.r; };
const auto decR = [](const Task& a, const Task& b)->bool { return a.r < b.r; };
const auto incQ = [](const Task& a, const Task& b)->bool { return a.q > b.q; };
const auto decQ = [](const Task& a, const Task& b)->bool { return a.q < b.q; };